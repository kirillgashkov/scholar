FROM ubuntu:20.04 AS texlive-builder

ENV TEXLIVE_REPOSITORY="https://texlive.info/CTAN/systems/texlive/tlnet"

# ca-certificates is used by curl.
# perl-modules is used by install-tl.
# gnupg2 is used by tlmgr.
# fontconfig is used by package xetex and maybe something else.
# The /opt/texlive/bin/current symlink is created to not rely on the name of the current platform.
# See https://www.tug.org/texlive/quickinstall.html.
# See https://yihui.org/tinytex/faq/.
RUN apt-get update \
 && apt-get install --no-install-recommends -y curl ca-certificates perl gnupg2 fontconfig \
 && rm -rf /var/lib/apt/lists/* \
 && cd /tmp \
 && echo > texlive.profile "selected_scheme scheme-infraonly" \
 && echo >> texlive.profile "TEXDIR /opt/texlive" \
 && echo >> texlive.profile "TEXMFLOCAL /opt/texlive/texmf-local" \
 && echo >> texlive.profile "TEXMFSYSCONFIG /opt/texlive/texmf-config" \
 && echo >> texlive.profile "TEXMFSYSVAR /opt/texlive/texmf-var" \
 && echo >> texlive.profile "instopt_portable 1" \
 && echo >> texlive.profile "tlpdbopt_autobackup 0" \
 && echo >> texlive.profile "tlpdbopt_install_docfiles 0" \
 && echo >> texlive.profile "tlpdbopt_install_srcfiles 0" \
 && curl -sSL "$TEXLIVE_REPOSITORY/install-tl-unx.tar.gz" | tar -vxz \
 && ./install-tl-*/install-tl --repository "$TEXLIVE_REPOSITORY" --profile texlive.profile \
 && ln -vsT "$(basename /opt/texlive/bin/*)" /opt/texlive/bin/current \
 && rm -vrf /tmp/*

ENV PATH="$PATH:/opt/texlive/bin/current"

# See https://tex.stackexchange.com/q/547708.
RUN tlmgr update --self \
    # scheme-minimal provides TeX.
    # latex-bin provides LaTeX and LuaLaTeX.
    # xetex provides XeLaTeX.
 && tlmgr install scheme-minimal \
                  latex-bin \
                  xetex \
    # Packages are used by the template via \documentclass, \usepackage and somewhat else.
    # The list can acquired by passing \listfiles output to kpsewhich.
    # \listfiles is a LaTeX command that should be added to the preamble. It outputs to the log file.
    # kpsewhich is a CLI command that accepts a file (e.g. extarticle.cls) and outputs its location.
    # This can be used to find the CTAN package name.
    #
    # kpsewhich output processing:
    # 1. filter internal files,
    # 2. remove common prefixes to get generic/<ctan-name>/... or latex/<ctan-name>/...,
    # 3. use the second path component as the CTAN package name.
    #
    # Tip: the latex/base is not a CTAN package, it is a kind of built-in.
 && tlmgr install amsfonts \
                  amsmath \
                  appendix \
                  auxhook \
                  babel \
                  babel-english \
                  babel-russian \
                  biblatex \
                  bigintcalc \
                  bitset \
                  caption \
                  catchfile \
                  csquotes \
                  enumitem \
                  environ \
                  eso-pic \
                  etexcmds \
                  etoolbox \
                  extsizes \
                  fancyvrb \
                  float \
                  fontspec \
                  framed \
                  fvextra \
                  geometry \
                  gettitlestring \
                  graphics \
                  graphics-cfg \
                  graphics-def \
                  hycolor \
                  hyperref \
                  ifplatform \
                  iftex \
                  infwarerr \
                  intcalc \
                  kvdefinekeys \
                  kvoptions \
                  kvsetkeys \
                  l3backend \
                  l3kernel \
                  l3packages \
                  letltxmacro \
                  lineno \
                  logreq \
                  ltxcmds \
                  makecell \
                  mathtools \
                  minted \
                  multirow \
                  needspace \
                  newfloat \
                  pdfcol \
                  pdfescape \
                  pdflscape \
                  pdfpages \
                  pdftexcmds \
                  pgf \
                  placeins \
                  refcount \
                  rerunfilecheck \
                  soul \
                  stringenc \
                  tcolorbox \
                  tikzfill \
                  titlesec \
                  tocloft \
                  tools \
                  trimspaces \
                  ulem \
                  unicode-math \
                  uniquecounter \
                  upquote \
                  url \
                  xcolor \
                  xstring \
    # biber is used by biblatex.
    # latexmk is used to compile documents.
 && tlmgr install biber \
                  latexmk


FROM python:3.11-slim AS base

ENV UID=1000 GID=1000 HOME=/user
ENV APP="$HOME/app"
RUN groupadd --gid "$GID" user \
 && useradd --uid "$UID" --gid "$GID" --home-dir "$HOME" --shell /bin/bash user \
 && mkdir "$HOME" \
 && chown "$UID:$GID" "$HOME" \
 && mkdir "$APP" \
 && chown "$UID:$GID" "$APP"

WORKDIR "$APP"


FROM base AS app-builder

USER "$UID:$GID"

COPY requirements.txt ./
RUN python -m venv .venv \
 && . .venv/bin/activate \
 && pip install --no-deps --require-hashes --constraint requirements.txt --requirement requirements.txt


FROM base AS runner

# perl is used by latexmk.
RUN apt-get update \
 && apt-get install --no-install-recommends -y curl librsvg2-bin perl \
 && rm -rf /var/lib/apt/lists/* \
 # Used Panflute version (in pyproject.toml) and Pandoc version (here, in
 # Dockerfile) must be compatible with each other in the Pandoc API they use.
 && curl -sSL "https://github.com/jgm/pandoc/releases/download/3.1.13/pandoc-3.1.13-linux-amd64.tar.gz" | tar -xz -C /usr/local/bin --strip-components 2 pandoc-3.1.13/bin/pandoc \
 && mkdir -p /usr/share/fonts/opentype \
 && mkdir /usr/share/fonts/opentype/xits \
 && curl -sSL "https://github.com/aliftype/xits/raw/e2c551983514dfb5e95cf5e6e5754ba9bae4a367/XITS-Bold.otf" > /usr/share/fonts/opentype/xits/XITS-Bold.otf \
 && curl -sSL "https://github.com/aliftype/xits/raw/e2c551983514dfb5e95cf5e6e5754ba9bae4a367/XITS-BoldItalic.otf" > /usr/share/fonts/opentype/xits/XITS-BoldItalic.otf \
 && curl -sSL "https://github.com/aliftype/xits/raw/e2c551983514dfb5e95cf5e6e5754ba9bae4a367/XITS-Italic.otf" > /usr/share/fonts/opentype/xits/XITS-Italic.otf \
 && curl -sSL "https://github.com/aliftype/xits/raw/e2c551983514dfb5e95cf5e6e5754ba9bae4a367/XITS-Regular.otf" > /usr/share/fonts/opentype/xits/XITS-Regular.otf \
 && curl -sSL "https://github.com/aliftype/xits/raw/e2c551983514dfb5e95cf5e6e5754ba9bae4a367/XITSMath-Bold.otf" > /usr/share/fonts/opentype/xits/XITSMath-Bold.otf \
 && curl -sSL "https://github.com/aliftype/xits/raw/e2c551983514dfb5e95cf5e6e5754ba9bae4a367/XITSMath-Regular.otf" > /usr/share/fonts/opentype/xits/XITSMath-Regular.otf
RUN mkdir /usr/share/fonts/opentype/freefont \
 && curl -sSL "https://ftp.gnu.org/gnu/freefont/freefont-otf-20120503.tar.gz" | tar -vxz -C /usr/share/fonts/opentype/freefont --strip-components 1 --wildcards "*.otf"

USER "$UID:$GID"

COPY . ./
COPY --from=app-builder "$APP/.venv" .venv
COPY --from=texlive-builder /opt/texlive /opt/texlive

ENV PATH="$APP/.venv/bin:/opt/texlive/bin/current:$PATH" PYTHONPATH="$APP"
WORKDIR "$HOME/workdir"

ENTRYPOINT ["python", "-m", "scholar"]
