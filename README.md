# Scholar

Write academic articles in Markdown.

## Requirements

### Scholar requirements

- [Python 3.11](https://www.python.org/)
- [Pandoc 2.19](https://github.com/jgm/pandoc)
- [LaTeX](https://www.latex-project.org/)
- [Librsvg](https://wiki.gnome.org/Projects/LibRsvg) (if you want to use SVG
  images)

### GOST style requirements

- [XITS](https://github.com/aliftype/xits)
- [FreeFont](https://www.gnu.org/software/freefont/)

## Installation

### Docker

Install Scholar and all of its dependencies by running:

```sh
$ docker build --tag scholar .
```

Test the installation by running:

```sh
$ docker run --rm -it scholar --help
```

### Docker Compose

Alternatively you can use Docker Compose to achieve the same result. Install by
running:

```sh
$ docker compose build scholar
```

Test the installation by running:

```sh
$ docker compose run --rm -it scholar --help
```

### Manual

Install external dependencies from the requirements section, clone the repository, preferably create a new virtual environment, then run:

```sh
$ pip install .
```

Test the installation by running:

```
$ scholar --help
```

### Manual (development)

Install external dependencies from the requirements section, clone the repository, create a new virtual environment, then run:

```sh
$ source /path/to/venv/bin/activate
$ pip install pip-tools
$ make pip-sync
```

Test the installation by running:

```
$ python -m scholar --help
```

## Usage

Write a Markdown document, run `scholar document.md`, enjoy `document.pdf`. When in doubt, consult `scholar --help`. Some notable usage details include:

- Scholar supports multiple styles out of the box. Run `scholar --styles` to see supported styles. You can choose a style by using `--style <style>` option like this: `scholar --style gost_thesis document.md`.
- Scholar supports title pages. Use `--title-page path/to/title-page.pdf` to prepend title pages to your document like this: `scholar --title-page title-page.pdf document.md`. You can have more than 1 PDF page in this file.
- Scholar creates a `$PWD/.scholar` directory when you run it. This directory contains Scholar's cache that allows it to quickly rebuild your documents. It can get big, so be sure to remove it when you are done writing your document. If you are getting obscure errors, deleting the directory and rebuilding the document sometimes can help.
- Scholar allows you to manually adjust generated LaTeX file before compiling it into PDF. Use option `--to-tex` to convert Markdown to LaTeX and `--from-tex` to convert LaTeX to PDF like this: `scholar --to-tex document.md -o document.tex`

### Docker

```sh
# Option --volume's value makes your current working directory accessible from within Docker container.
# Option --user's value makes Scholar use the same user within the container so that it could access your working directory.
# Option --env's value helps Scholar to not freak out when *your* user does not exist inside of the container by setting HOME to a *writable* directory.
$ docker run --rm -it \
    --volume "$(pwd):/user/workdir" \
    --user "$(id -u):$(id -g)" \
    --env HOME=/tmp \
    scholar \
    document.md \
    -o document.pdf
```

### Docker Compose

Put the Compose file into the directory of your document, then run:

```sh
$ docker compose run --rm -it scholar document.md -o document.pdf
```

### Manual

```sh
$ scholar document.md -o document.pdf
```

### Manual (development)

```sh
# The new PYTHONPATH allows you to run Scholar from any directory.
$ PYTHONPATH="/path/to/repository/of/scholar:$PYTHONPATH" \
    python \
    -m scholar \
    document.md \
    -o document.pdf
```

## Examples

See the [examples](examples) directory.

## License

Distributed under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
