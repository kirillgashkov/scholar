from pathlib import Path
from typing import Any

from scholar.constants import PANDOC_TEMPLATE_FILE
from scholar.settings import Settings


class Style:
    def __init__(
        self,
        *,
        template_file: Path,
        # filter_files: list[Path],  # Not supported yet
        variables: dict[str, Any],
    ):
        self.template_file = template_file
        # self.filter_files = filter_files  # Not supported yet
        self.variables = variables


class GostStyle(Style):
    def __init__(
        self,
        *,
        # title_page: Path | None,  # Implemented somewhere else for now.
        disable_main_section_numbering: bool,
        disable_section_page_breaks: bool,
        disable_numbering_within_section: bool,
        enable_centered_section_headings: bool,
        # enable_uppercase_section_headings: bool,  # Not implemnted. To implement it with need to adopt `titletoc`. Right now the workaround is to type out the headings in uppercase.
        enable_uppercase_appendix_name: bool,
        content_start_page_number: int | None,
        codeblock__use_old_baselinestretch: bool,
    ) -> None:
        super().__init__(
            template_file=PANDOC_TEMPLATE_FILE,
            variables={
                # "title_page": title_page,  # Implemented somewhere else for now.
                "disable_main_section_numbering": disable_main_section_numbering,
                "disable_section_page_breaks": disable_section_page_breaks,
                "disable_numbering_within_section": disable_numbering_within_section,
                "enable_centered_section_headings": enable_centered_section_headings,
                # "enable_uppercase_section_headings": enable_uppercase_section_headings,  # Not implemnted. To implement it with need to adopt `titletoc`. Right now the workaround is to type out the headings in uppercase.
                "enable_uppercase_appendix_name": enable_uppercase_appendix_name,
                "content_start_page_number": content_start_page_number,
                "codeblock__use_old_baselinestretch": codeblock__use_old_baselinestretch,
            },
        )


class GostThesisStyle(GostStyle):
    def __init__(
        self,
        # *,
        # title_page: Path | None = None,  # Implemented somewhere else for now.
    ) -> None:
        super().__init__(
            # title_page=title_page,  # Implemented somewhere else for now.
            disable_main_section_numbering=False,
            disable_section_page_breaks=False,
            disable_numbering_within_section=False,
            enable_centered_section_headings=False,
            # enable_uppercase_section_headings=False,  # Not implemnted. To implement it with need to adopt `titletoc`. Right now the workaround is to type out the headings in uppercase.
            enable_uppercase_appendix_name=False,
            content_start_page_number=None,
            codeblock__use_old_baselinestretch=False,
        )


class GostMireaThesisStyle(GostStyle):
    def __init__(
        self,
        # *,
        # title_page: Path | None = None,  # Implemented somewhere else for now.
    ) -> None:
        super().__init__(
            # title_page=title_page,  # Implemented somewhere else for now.
            disable_main_section_numbering=False,
            disable_section_page_breaks=False,
            disable_numbering_within_section=False,
            enable_centered_section_headings=True,
            # enable_uppercase_section_headings=False,  # Not implemnted. To implement it with need to adopt `titletoc`. Right now the workaround is to type out the headings in uppercase.
            enable_uppercase_appendix_name=True,
            content_start_page_number=None,
            codeblock__use_old_baselinestretch=False,
        )


class GostMireaReportStyle(GostStyle):
    def __init__(
        self,
        # *,
        # title_page: Path | None = None,  # Implemented somewhere else for now.
    ) -> None:
        super().__init__(
            # title_page=title_page,  # Implemented somewhere else for now.
            disable_main_section_numbering=True,
            disable_section_page_breaks=True,
            disable_numbering_within_section=True,
            enable_centered_section_headings=True,
            # enable_uppercase_section_headings=False,  # Not implemnted. To implement it with need to adopt `titletoc`. Right now the workaround is to type out the headings in uppercase.
            enable_uppercase_appendix_name=True,
            content_start_page_number=None,
            codeblock__use_old_baselinestretch=False,
        )


class GostMireaThesisOldStyle(GostStyle):
    def __init__(
        self,
        # *,
        # title_page: Path | None = None,  # Implemented somewhere else for now.
    ) -> None:
        super().__init__(
            # title_page=title_page,  # Implemented somewhere else for now.
            disable_main_section_numbering=False,
            disable_section_page_breaks=False,
            disable_numbering_within_section=False,
            enable_centered_section_headings=True,
            # enable_uppercase_section_headings=False,  # Not implemnted. To implement it with need to adopt `titletoc`. Right now the workaround is to type out the headings in uppercase.
            enable_uppercase_appendix_name=True,
            content_start_page_number=None,
            codeblock__use_old_baselinestretch=True,
        )


class GostReportStyle(GostStyle):
    def __init__(
        self,
        # *,
        # title_page: Path | None = None,  # Implemented somewhere else for now.
    ) -> None:
        super().__init__(
            # title_page=title_page,  # Implemented somewhere else for now.
            disable_main_section_numbering=True,
            disable_section_page_breaks=True,
            disable_numbering_within_section=True,
            enable_centered_section_headings=False,
            # enable_uppercase_section_headings=False,  # Not implemnted. To implement it with need to adopt `titletoc`. Right now the workaround is to type out the headings in uppercase.
            enable_uppercase_appendix_name=False,
            content_start_page_number=None,
            codeblock__use_old_baselinestretch=False,
        )


class GostBigReportStyle(GostStyle):
    def __init__(
        self,
        # *,
        # title_page: Path | None = None,  # Implemented somewhere else for now.
    ) -> None:
        super().__init__(
            # title_page=title_page,  # Implemented somewhere else for now.
            disable_main_section_numbering=True,
            disable_section_page_breaks=False,
            disable_numbering_within_section=True,
            enable_centered_section_headings=False,
            # enable_uppercase_section_headings=False,  # Not implemnted fully. Right now it affects only template's constant strings. To implement it we need to adopt `titletoc`.
            enable_uppercase_appendix_name=False,
            content_start_page_number=None,
            codeblock__use_old_baselinestretch=False,
        )


DEFAULT_STYLE = "gost_thesis"


def get_styles() -> dict[str, Style]:
    return {
        "gost_thesis": GostThesisStyle(),
        "gost_mirea_thesis": GostMireaThesisStyle(),
        "gost_mirea_report": GostMireaReportStyle(),
        "gost_mirea_thesis_old": GostMireaThesisOldStyle(),
        "gost_report": GostReportStyle(),
        "gost_big_report": GostBigReportStyle(),
    }


def get_style(settings: Settings) -> Style:
    return get_styles()[settings.style]
