return {
  {
    CodeBlock = function(
      code_block_el -- pandoc.CodeBlock
    )
      if code_block_el.attr.classes:includes("raw") then
        return pandoc.RawBlock("latex", code_block_el.text)
      else
        return code_block_el
      end
    end,
  },
  {
    Code = function(
      code_el -- pandoc.Code
    )
      if code_el.attr.classes:includes("raw") then
        return pandoc.RawInline("latex", code_el.text)
      else
        return code_el
      end
    end,
  },
}
