local orderedlist = {}

---@param l pandoc.OrderedList
---@return pandoc.OrderedList
function orderedlist.Reset(l)
  -- The start number is Commonmark-compliant but the style and delimiter are
  -- not.
  l.listAttributes = pandoc.ListAttributes(l.listAttributes.start, "DefaultStyle", "DefaultDelim")
  return l
end

-- I'd prefer this filter to exist in other form that is why we don't set
-- decimal and one paren in the reset function.
---@param l pandoc.OrderedList
---@return pandoc.OrderedList
function orderedlist.SetListStyleTypeDefault(l)
  l.listAttributes.style = "Decimal"
  l.listAttributes.delimiter = "OneParen"
  return l
end

---@param d pandoc.Div
---@return pandoc.Div
function orderedlist.SetListStyleTypeFromDiv(d)
  local style = nil
  local delimiter = nil
  for _, c in ipairs(d.attr.classes) do
    if c == "list-decimal-period" then
      style = "Decimal"
      delimiter = "Period"
    elseif c == "list-decimal-paren" or c == "list-decimal-round" then
      style = "Decimal"
      delimiter = "OneParen"
    end
  end

  if style ~= nil and delimiter ~= nil then
  elseif style == nil and delimiter == nil then
    return d
  else
    assert(false)
  end
  ---@cast style -nil
  ---@cast delimiter -nil

  d = pandoc.walk_block(d, {
    ---@param l pandoc.OrderedList
    OrderedList = function(l)
      l.listAttributes.style = style
      l.listAttributes.delimiter = delimiter
      return l
    end,
  })
  return d
end

return {
  {
    ---@param l pandoc.OrderedList
    OrderedList = function(l)
      l = orderedlist.Reset(l)
      l = orderedlist.SetListStyleTypeDefault(l)
      return l
    end,
  },
  {
    ---@param d pandoc.Div
    Div = function(d)
      return orderedlist.SetListStyleTypeFromDiv(d)
    end,
  },
}
