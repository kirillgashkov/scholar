---@param path string
---@return boolean
local function fileExists(path)
  local f = io.open(path)
  if f then
    f:close()
  end
  return f ~= nil
end

return {
  {
    ---@param e pandoc.CodeBlock
    ---@return any
    CodeBlock = function(e)
      if not e.attr.classes:includes("techmark") then
        return e
      end

      local scriptDir = pandoc.path.directory(PANDOC_SCRIPT_FILE)
      local techmarkDir = pandoc.path.join({ scriptDir, "techmark" })
      if not fileExists(pandoc.path.join({ techmarkDir, "README.md" })) then
        io.stderr:write("error: it appears that the techmark git submodule is not initialized\n")
        return e
      end

      local luaPath = (
        pandoc.path.join({ techmarkDir, "src", "?.lua" })
        .. ";"
        .. pandoc.path.join({ techmarkDir, "src", "?/init.lua" })
        .. ";;"
      )
      local techmarkLatex = pandoc.pipe("/usr/bin/env", {
        ("LUA_PATH" .. "=" .. luaPath),
        "pandoc",
        "--read",
        pandoc.path.join({ techmarkDir, "src", "reader", "reader.lua" }),
        "--write",
        pandoc.path.join({ techmarkDir, "src", "writer", "writer.lua" }),
      }, e.text)

      return pandoc.RawBlock("latex", techmarkLatex)
    end,
  },
}
