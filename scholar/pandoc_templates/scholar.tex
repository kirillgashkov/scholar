\documentclass[14pt]{extarticle}


%
% Environment
%

% Sets document's primary and secondary languages.
% Here the primary language is Russian because it is specified last.
\usepackage[shorthands=off,english,russian]{babel}

% Sets Russian and English alphabets for letter-based counters.
% The letters Ё, З, Й, О, Ч, Ъ, Ы, Ь are skipped.
% See "ГОСТ Р 2.105-2019" p. 6.3.5.
\makeatletter
\def\russian@alph#1{\ifcase#1\or а\or б\or в\or г\or д\or е\or ж\or и\or к\or л\or м\or н\or п\or р\or с\or т\or у\or ф\or х\or ц\or ш\or щ\or э\or ю\or я\else\@ctrerr\fi}
\def\russian@Alph#1{\ifcase#1\or А\or Б\or В\or Г\or Д\or Е\or Ж\or И\or К\or Л\or М\or Н\or П\or Р\or С\or Т\or У\or Ф\or Х\or Ц\or Ш\or Щ\or Э\or Ю\or Я\else\@ctrerr\fi}
\makeatother

% Sets Russian and English alphabets for enumerations.
% The letters I, O are skipped.
% See "ГОСТ Р 2.105-2019" p. 6.3.5.
\makeatletter
\def\@alph#1{\ifcase#1\or a\or b\or c\or d\or e\or f\or g\or h\or j\or k\or l\or m\or n\or p\or q\or r\or s\or t\or u\or v\or w\or x\or y\or z\else\@ctrerr\fi}
\def\@Alph#1{\ifcase#1\or A\or B\or C\or D\or E\or F\or G\or H\or J\or K\or L\or M\or N\or P\or Q\or R\or S\or T\or U\or V\or W\or X\or Y\or Z\else\@ctrerr\fi}
\makeatother


%
% Layout
%

\usepackage{geometry}
\geometry{paper=a4paper}
\geometry{left=3cm,right=1cm,top=2cm,bottom=2cm}


%
% Font
%

\usepackage{fontspec}
\setmainfont{XITS} % open source Times New Roman
\setmonofont{FreeMono} % open source Courier New

% Provides \uline for underline and \sout for strikeout.
\usepackage[normalem]{ulem}

% Provides \st for strikeout.
\usepackage{soul}

% Provides \enquote for language-aware quoting.
% E.g. \enquote{text} is equivalent to «text» in Russian.
% Used by Pandoc to render straight quotes ("") as language-specific quotes (e.g. «» in Russian).
\usepackage{csquotes}


%
% Paragraph
%

\usepackage{indentfirst}

% Sets the first line indent of a paragraph.
\setlength\parindent{1.25cm}

% Sets the line spacing to Microsoft Word's 1.5.
\linespread{1.424}

% Justifies text.
\tolerance=1
\emergencystretch=\maxdimen
\hbadness=10000
\frenchspacing

% Defines commands for enabling and disabling hyphenation on demand.
% See https://tex.stackexchange.com/a/52009.
\makeatletter
\newcommand{\template@hyphenation@enable}{%
  \hyphenpenalty=50%
  \exhyphenpenalty=50%
}
\newcommand{\template@hyphenation@disable}{%
  \hyphenpenalty=10000%
  \exhyphenpenalty=10000%
}
\newcommand{\template@hyphenation@save}{%
  \mathchardef\template@hyphenation@internal@hyphenpenalty=\hyphenpenalty%
  \mathchardef\template@hyphenation@internal@exhyphenpenalty=\exhyphenpenalty%
}
\newcommand{\template@hyphenation@restore}{%
  \hyphenpenalty=\template@hyphenation@internal@hyphenpenalty%
  \exhyphenpenalty=\template@hyphenation@internal@exhyphenpenalty%
}
\makeatother

% Disables hyphenation by default.
% Hyphenation is temporary enabled in code blocks and tables.
\makeatletter
\template@hyphenation@disable
\makeatother


%
% Bullet list, ordered list
%

% Provides commands for styling lists.
\usepackage{enumitem}

% Teaches package enumitem the Russian alphabet for ordered lists.
\makeatletter
\AddEnumerateCounter{\asbuk}{\russian@alph}{щ}
\AddEnumerateCounter{\Asbuk}{\russian@Alph}{Ж}
\makeatother

% Sets common settings for all lists.
\setlist{labelindent=\parindent,leftmargin=*,listparindent=\parindent,topsep=0pt,partopsep=0pt,parsep=\parskip,itemsep=0pt}

% Sets additional settings for bullet lists.
\setlist[itemize]{label={$$\bullet$$}}

% Sets additional settings for ordered lists.
% When using with Pandoc, a Lua filter takes effect instead.
\setlist[enumerate,1]{label={\arabic*)}}
\setlist[enumerate,2]{label={\asbuk*)}}
\setlist[enumerate,3]{label={\arabic*)}}
\setlist[enumerate,4]{label={\asbuk*)}}

% Provides a no-op \tightlist for Pandoc.
% It is used by Pandoc to render loose and tight lists differently, but we render all lists the same.
\newcommand{\tightlist}{}


%
% Header
%

\usepackage{titlesec}
\usepackage[titletoc]{appendix}

\makeatletter
\newenvironment{scholar@section@main}{%
  $if(scholar.variables.disable_main_section_numbering)$\makeatletter\c@secnumdepth=0\makeatother$else$$endif$%  % (Conditionally) disable section numbering. Using \c@secnumdepth=0 instead of \setcounter{secnumdepth}{0} to have the counter restored at the end of the environment. (See the comments to https://tex.stackexchange.com/a/11669.)
  $if(scholar.variables.enable_centered_section_headings)$%
  \titleformat{\section}[block]{\filcenter\large\fontsize{16}{19.2}\selectfont\bfseries}{\thesection.}{0.5em}{}%  % Using "\large\fontsize{16}{19.2}\selectfont" instead of "\large" to have an exact 16pt level 1 heading. The second argument to fontsize is 16 times 1.2. See https://tex.stackexchange.com/questions/48276/latex-specify-font-point-size.
  \titlespacing{\section}{0pt}{15pt}{10pt}%
  %
  \titleformat{\subsection}[block]{\filcenter\normalsize\bfseries}{\thesubsection.}{0.5em}{}%
  \titlespacing{\subsection}{0pt}{15pt}{10pt}%
  %
  \titleformat{\subsubsection}[block]{\filright\normalsize\bfseries\itshape}{\thesubsubsection.}{0.5em}{}%
  \titlespacing{\subsubsection}{0pt}{15pt}{10pt}%
  $else$%
  \titleformat{\section}[block]{\filright\large\fontsize{16}{19.2}\selectfont\bfseries}{\thesection}{0.5em}{}%
  \titlespacing{\section}{\parindent}{3ex}{2ex}%
  %
  \titleformat{\subsection}[block]{\filright\normalsize\bfseries}{\thesubsection}{0.5em}{}%
  \titlespacing{\subsection}{\parindent}{3ex}{2ex}%
  %
  \titleformat{\subsubsection}[block]{\filright\normalsize\bfseries\itshape}{\thesubsubsection}{0.5em}{}%
  \titlespacing{\subsubsection}{\parindent}{3ex}{2ex}%
  $endif$%
}{}
\makeatother

\makeatletter
\newenvironment{scholar@section@side}{%
  \makeatletter\c@secnumdepth=0\makeatother%  % Disable section numbering. Using \c@secnumdepth=0 instead of \setcounter{secnumdepth}{0} to have the counter restored at the end of the environment. (See the comments to https://tex.stackexchange.com/a/11669.)
  \titleformat{\section}[block]{\filcenter\large\fontsize{16}{19.2}\selectfont\bfseries}{}{0pt}{}%
  \titlespacing{\section}{0pt}{15pt}{10pt}%
  %
  \titleformat{\subsection}[block]{\filcenter\normalsize\bfseries}{}{0pt}{}%
  \titlespacing{\subsection}{0pt}{15pt}{10pt}%
  %
  \titleformat{\subsubsection}[block]{\filright\normalsize\bfseries\itshape}{}{0pt}{}%
  \titlespacing{\subsubsection}{0pt}{15pt}{10pt}%
}{}
\makeatother

\newcommand{\nocontentsline}[3]{}

\makeatletter
\newenvironment{scholar@section@contents}{%
  \let\addcontentsline=\nocontentsline%  % See https://stackoverflow.com/q/2785260.
  \scholar@section@side%
}{%
  \endscholar@section@side%
}
\makeatother

\makeatletter
\newenvironment{scholar@section@appendix}{%
  \appendices%
  \renewcommand{\thesection}{\Asbuk{section}}%
$if(scholar.variables.enable_uppercase_appendix_name)$
  % Changes the text that appears in the appendix section headings from
  % "Appendix A" to "APPENDIX A".
  %
  % See https://mirror.truenetwork.ru/CTAN/macros/latex/contrib/appendix/appendix.pdf (p. 3).
  \renewcommand{\appendixname}{ПРИЛОЖЕНИЕ}
$endif$
  %
  \titleformat{\section}[display]{\filcenter\large\fontsize{16}{19.2}\selectfont\bfseries}{\appendixname~\thesection}{2ex}{\vspace{-0.5\baselineskip}}%
  \titlespacing{\section}{0pt}{15pt}{10pt}%
  %
  \titleformat{\subsection}[block]{\filcenter\normalsize\bfseries}{\thesubsection.}{0.5em}{}%
  \titlespacing{\subsection}{0pt}{15pt}{10pt}%
  %
  \titleformat{\subsubsection}[block]{\filright\normalsize\bfseries\itshape}{\thesubsubsection.}{0.5em}{}%
  \titlespacing{\subsubsection}{0pt}{15pt}{10pt}%
}{%
  \endappendices%
}
\makeatother


%
% Image
%

% Provides the \includegraphics command.
\usepackage{graphicx}

% Provides commands for caption styling.
\usepackage{caption}

% Sets the default \includegraphics options.
%
% Tell LaTeX to not upscale images that fit and to downscale images that don't;
% downscaled images will keep their aspect ratio and have a width of 3/4 of the
% current paragraph's width; extremely narrow images will probably overflow,
% therefore you should set their height manually by overriding the defaults)
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>0.75\linewidth 0.75\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight \textheight\else\Gin@nat@height\fi}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\makeatother

% Allows figures to be placed [h]ere, at [t]op, [b]ottom or on a special [p]age.
% Where exactly to put them is up to LaTeX's float algorithm.
\usepackage{float}
\makeatletter
\def\fps@figure{H}
\makeatother

% Sets the name to be used for the figure caption.
% Needs package babel.
\addto\captionsrussian{\renewcommand{\figurename}{Рисунок}}

% Sets the vertical spacing before and after floats.
% Pictures are typically rendered as floats.
\setlength{\textfloatsep}{\dimexpr \baselineskip - 1.6ex + \baselineskip \relax} % WTF: For some reason in contrary to [h]ere floats [t]op floats don't have extra vertical spacing between the caption and the text, but since we remove this extra spacing for all floats during \captionsetup, we need to return the removed spacing for [t]op floats here.
\setlength{\floatsep}{\dimexpr \baselineskip - 1.6ex + 1.2\baselineskip \relax} % WTF: This length controls the spacing between two subsequent [t]op (or [b]ottom) floats. Because during \captionsetup we remove some of the vertical spacing around captions to make [h]ere floats look better with the adjacent main text and because there is no main text between two subsequent floats, we need to return the removed spacing here.
\setlength{\intextsep}{\baselineskip}

% Makes floats always appear after their definition.
% Pictures are typically rendered as floats.
\usepackage{flafter}

% Sets restrictions for the float-placing algorithm.
% We disallow small floats on float-only pages.
% Pictures are typically rendered as floats.
\renewcommand{\topfraction}{0.75}
\renewcommand{\floatpagefraction}{0.70}

% Prevent floats from crossing section divisions and start each section on a new
% page.
%
% Actually calling \clearpage is enough to cause the unplaced floats to be
% placed but below we also call \FloatBarrier from the "placeins" package, all
% in attempt to make the final output look better. The problem lies in
% float-only pages and how \clearpage forces the floats on them. Without the
% sophisticated mechanism below you can find yourself with pages consisting of 1
% tiny float in the center which is definitely unsatisfactory.
%
% When a new section is about to start, we first call \FloatBarrier. It "nicely"
% asks LaTeX's float algorithm to place the floats it has with (often with than
% without) respect to restrictions. A tiny figure wouldn't be placed just yet
% but a huge figure would and it would be vertically centered. After
% \FloatBarrier did its job we temporary change the way floats on float-only
% pages are displayed: floats on these pages will appear at the top, not in the
% center. Then we force the rest of the floats to be placed by calling
% \clearpage.
%
% In rare cases, for example when you have a tiny figure and a huge figure at
% the end of a section, you still can get unsatisfactory results, like the ones
% described above, but you can fix them if you play with figure sizes a little.
% Generally this mechanism shouldn't cause any problems.
%
% See https://tex.stackexchange.com/a/39020 to find more about LaTeX's
% float-placing algorithm.
%
% Pictures are typically rendered as floats.
\usepackage{placeins}
\usepackage{etoolbox}
\makeatletter
\newlength{\old@fptop}
\newlength{\old@fpsep}
\newlength{\old@fpbot}
\pretocmd{\section}{%
  \FloatBarrier%
  % Save original values.
  \setlength{\old@fptop}{\@fptop}%
  \setlength{\old@fpsep}{\@fpsep}%
  \setlength{\old@fpbot}{\@fpbot}%
  % Push floats on float-only pages to the top.
  \setlength{\@fptop}{0pt}%
  \setlength{\@fpsep}{1.2\baselineskip}%
  \setlength{\@fpbot}{0pt plus 1fil}%
  $if(scholar.variables.disable_section_page_breaks)$$else$\clearpage$endif$%
  % Restore original values.
  \setlength{\@fptop}{\old@fptop}%
  \setlength{\@fpsep}{\old@fpsep}%
  \setlength{\@fpbot}{\old@fpbot}%
}{}{\GenericError{}{Command patching failed}{}{}}
\makeatother

% Prevents floats from crossing subsection and subsubsection divisions.
% Since we don't call \clearpage for them, the handling is a bit easier.
% Pictures are typically rendered as floats.
\pretocmd{\subsection}{\FloatBarrier}{}{\GenericError{}{Command patching failed}{}{}}
\pretocmd{\subsubsection}{\FloatBarrier}{}{\GenericError{}{Command patching failed}{}{}}

% Configures the format of captions for figures.
% Needs package caption.
\DeclareCaptionLabelSeparator{templateDash}{ -- }
\captionsetup[figure]{
  format=plain,
  labelformat=simple,
  labelsep=templateDash,
  justification=centering,
  width=0.75\linewidth, % if \captionsetup didn't have the [figure] option, the \linewidth would be smaller than it should be
  position=below,
  aboveskip=0.5\baselineskip,
  belowskip=\dimexpr -\baselineskip + 1.6ex \relax, % remove the extra vertical space from the bottom of figures, see https://tex.stackexchange.com/q/32614
}

$if(scholar.variables.disable_numbering_within_section)$
$else$
\counterwithin{figure}{section}
$endif$


%
% Table
%

\usepackage{longtable} % Used in LaTeX tables generated by a custom "make_latex_table" Lua filter
\usepackage{array} % Used in LaTeX tables generated by a custom "make_latex_table" Lua filter
\usepackage{calc} % Used in LaTeX tables generated by a custom "make_latex_table" Lua filter
\usepackage{makecell} % Used in LaTeX tables generated by a custom "make_latex_table" Lua filter to horizontally center column heads
\usepackage[hidelinks]{hyperref} % Used in LaTeX tables generated by the "pandoc-crossref" filter % (See https://tex.stackexchange.com/q/823.)

\captionsetup[table]{
  format=plain,
  labelformat=simple,
  labelsep=templateDash,
  justification=justified,
  singlelinecheck=false,
  width=\linewidth,
  position=above,
  skip=0.5\baselineskip,
}

% "templateTableNumberedContinuation" style is used in LaTeX
% tables generated by a custom "make_latex_table" Lua filter
\DeclareCaptionLabelFormat{templateTableNumberedContinuation}{Продолжение таблицы #2}
\DeclareCaptionStyle{templateTableNumberedContinuation}{
  format=plain,
  labelformat=templateTableNumberedContinuation,
  labelsep=none,
  textformat=empty,
  justification=justified,
  singlelinecheck=false,
  width=\linewidth,
  position=above,
  skip=0.5\baselineskip,
}
% "templateTableUnnumberedContinuation" style is used in LaTeX
% tables generated by a custom "make_latex_table" Lua filter
\DeclareCaptionFormat{templateTableUnnumberedContinuation}{Продолжение таблицы#2#3\par}
\DeclareCaptionStyle{templateTableUnnumberedContinuation}{
  format=templateTableUnnumberedContinuation,
  labelformat=empty,
  labelsep=none,
  textformat=empty,
  justification=justified,
  singlelinecheck=false,
  width=\linewidth,
  position=above,
  skip=0.5\baselineskip,
}

\setlength{\LTpre}{\bigskipamount} % \bigskip above the longtables
\setlength{\LTpost}{\dimexpr \bigskipamount - \baselineskip + 1.6ex \relax} % WTF: \bigskip below the longtables but without the extra vertical space (see https://tex.stackexchange.com/q/32614)

% Unset some of the predefined 'makecell' settings for column heads so
% that their '\thead' macro just centered the text inside of the heads
\renewcommand\theadfont{}
\renewcommand\theadset{}
\renewcommand\theadgape{}
\renewcommand\rotheadgape{}

$if(scholar.variables.disable_numbering_within_section)$
$else$
\counterwithin{table}{section}
$endif$

% Provides \multirow for row spans.
% Option longtable patches package longtable to support \multirow. 
\usepackage[longtable]{multirow}

% Now goes definitions of two commands: \varhline and \varcline.
% They are based on \hline and \cline.
% They are used in exactly the same way as their originals except they accept line width as square bracket parameter whereas the originals don't.

% \varhline[#1] and \varcline[#1]#2-#3 are variable width \hline and
% \cline#1-#2. They are simple clones of original definitions with
% \arrayrulewidth replaced by #1. See https://tex.stackexchange.com/q/408868
% for original inspiration.

% Simple temporary \arrayrulewidth workaround from
% https://tex.stackexchange.com/a/24552 mostly worked but had problems when
% minted was imported. The problem is probably related to other commands trying
% to mess with \arrayrulewidth. booktabs was also considered, their \specialrule
% worked great but \cmidrule not so much, it had problems with vertical lines.

% \varvarhline is based on \hline from latex.ltx:12626-12633 and \varcline is
% based on \cline from latex.ltx12635-12646. \varvarhline is later used to
% define a longtable-compatible \varhline. \varcline is used directly in
% longtables but with a minor patch originating from multirow.
\makeatletter
\def\varvarhline[#1]{%
  \noalign{\ifnum0=`}\fi\hrule \@height #1 \futurelet
   \reserved@a\@xvarvarhline[#1]}
\def\@xvarvarhline[#1]{\ifx\reserved@a\varvarhline
               \vskip\doublerulesep
               \vskip-#1
             \fi
      \ifnum0=`{\fi}}
\def\varcline[#1]#2{\@varvarcline[#1]#2\@nil}
\def\@varvarcline[#1]#2-#3\@nil{%
  \omit
  \@multicnt#2%
  \advance\@multispan\m@ne
  \ifnum\@multicnt=\@ne\@firstofone{&\omit}\fi
  \@multicnt#3%
  \advance\@multicnt-#2%
  \advance\@multispan\@ne
  \leaders\hrule\@height#1\hfill
  \cr
  \noalign{\vskip-#1}}
\makeatother

% \varhline is based on \LT@hline from longtable.sty:369-388. It is used instead
% of \hline in longtables. We are using \varhline directly.
\makeatletter
\def\varhline[#1]{%
  \noalign{\ifnum0=`}\fi
    \penalty\@M
    \futurelet\@let@token\LT@@varvarhline[#1]}
\def\LT@@varvarhline[#1]{%
  \ifx\@let@token\varvarhline
    \global\let\@gtempa\@gobble
    \gdef\LT@sep{\penalty-\@medpenalty\vskip\doublerulesep}%
  \else
    \global\let\@gtempa\@empty
    \gdef\LT@sep{\penalty-\@lowpenalty\vskip-#1}%
  \fi
  \ifnum0=`{\fi}%
  \multispan\LT@cols
     \unskip\leaders\hrule\@height#1\hfill\cr
  \noalign{\LT@sep}%
  \multispan\LT@cols
     \unskip\leaders\hrule\@height#1\hfill\cr
  \noalign{\penalty\@M}%
  \@gtempa}
\makeatother

% Based on \@cline patch for longtable from multirow.sty:31-55. \@varvarcline is
% used by \varvarcline (similarly to how \@cline is used by \cline).
\makeatletter
\AtBeginDocument{%
\@ifundefined{CT@arc}
{\def\@varvarcline[#1]#2-#3\@nil{%
  \omit
  \@multicnt#2%
  \advance\@multispan\m@ne
  \ifnum\@multicnt=\@ne\@firstofone{&\omit}\fi
  \@multicnt#3%
  \advance\@multicnt-#2%
  \advance\@multispan\@ne
  \leaders\hrule\@height#1\hfill
  \cr
  \noalign{\nobreak\vskip-#1}}}
{\def\@varvarcline[#1]#2-#3\@nil{%
  \omit
  \@multicnt#2%
  \advance\@multispan\m@ne
  \ifnum\@multicnt=\@ne\@firstofone{&\omit}\fi
  \@multicnt#3%
  \advance\@multicnt-#2%
  \advance\@multispan\@ne
  {\CT@arc@\leaders\hrule\@height#1\hfill}%
  \cr
  \noalign{\nobreak\vskip-#1}}}
}
\makeatother


%
% Code block, code inline
%

% Needs package babel.
\addto\captionsrussian{\renewcommand{\listingname}{Листинг}}

% Provides \Needspace that reserves space to disallow page breaks before a certain point.
% Can be used like \Needspace{3\baselineskip}.
% See https://tex.stackexchange.com/a/28226.
\usepackage{needspace}

% Provides commands for code with syntax highlighting.
% "newfloat" option provides better integration with package caption.
\usepackage[newfloat]{minted}

% Provides longlisting environment for code listings that span multiple pages.
% Needs package minted.
% Needs package caption.
%
% Simple example:
%
%     \begin{longlisting}
%     \begin{minted}{python}
%     def greet(name):
%         print(f"Hello, {name}!")
%     \end{minted}
%     \caption{A function that greets a person}
%     \label{lst:greet}
%     \end{longlisting}
%
% File import example:
%
%     \begin{longlisting}
%     \inputminted{python}{hello.py}
%     \caption{A function that says hello}
%     \label{lst:hello}
%     \end{longlisting}
\newenvironment{longlisting}{\captionsetup{type=listing}}{}

\captionsetup[listing]{
  format=plain,
  labelformat=simple,
  labelsep=templateDash,
  justification=justified,
  singlelinecheck=false,
  width=\linewidth,
}

% Removes the default gap between normal text and 'minted' code block.
% See https://github.com/gpoore/minted/issues/256#issuecomment-605002404.
\fvset{listparameters=\setlength{\topsep}{0pt}\setlength{\partopsep}{0pt}}

% Enables hyphenation in minted code blocks so that minted's "breakanywhere" option could work properly.
\setminted{%
  breakanywhere,%
  breaklines,%
  fontsize=\small,%
$if(scholar.variables.codeblock__use_old_baselinestretch)$%
$else$%
  baselinestretch=1.2,%
$endif$%
  style=xcode%
}

% Adds frame around code blocks with proper margins.
\usepackage{tcolorbox}
\tcbuselibrary{skins,breakable}
\tcbset{templateCodeblock/.style={
  empty,
  breakable,
  borderline={0.4pt}{0pt}{black},
$if(scholar.variables.codeblock__use_old_baselinestretch)$%
  left=5pt, right=5pt,
  top=0pt, bottom=0pt,
$else$%
  left=3pt, right=3pt,
  top=0pt, bottom=0pt,
$endif$%
  boxsep=\fboxsep,
  sharp corners,
}}

% Enables hyphenation in code blocks.
% Long code lines will be broken into multiple lines.
\makeatletter
\BeforeBeginEnvironment{minted}{\template@hyphenation@save\template@hyphenation@enable}
\AfterEndEnvironment{minted}{\template@hyphenation@restore}
\makeatother

% Adds vertical space before and after code blocks.
% Needs to be before the tcolorbox wrapper, otherwise it won't work correctly.
\BeforeBeginEnvironment{minted}{\addvspace{0.5\baselineskip}}
\AfterEndEnvironment{minted}{\addvspace{0.5\baselineskip}}

% Wraps minted code blocks in a tcolorbox to add frame.
\BeforeBeginEnvironment{minted}{\begin{tcolorbox}[templateCodeblock]}
\AfterEndEnvironment{minted}{\end{tcolorbox}}

% Adds vertical space before and after longlistings.
\BeforeBeginEnvironment{longlisting}{\addvspace{0.25\baselineskip}}
\AfterEndEnvironment{longlisting}{\addvspace{0.75\baselineskip}}

$if(scholar.variables.disable_numbering_within_section)$
$else$
\counterwithin{listing}{section}
$endif$


%
% Math
%

% Provides math-related commands.
% Based on https://github.com/KaTeX/KaTeX/wiki/Package-Emulation.
% Note that not every KaTeX feature is supported by this template
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{mathtools}
\usepackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}

$if(scholar.variables.disable_numbering_within_section)$
$else$
\counterwithin{equation}{section}
$endif$


%
% Footnote
%

% Allows verbatim text in footnotes.
% Taken from Pandoc's template.tex.
\VerbatimFootnotes

% Increases footnote text font size.
% Taken from "extsizes/size17.clo".
% See https://tex.stackexchange.com/a/249422.
\makeatletter
\renewcommand\footnotesize{%
   \@setfontsize\footnotesize\@xiipt{14}%
   \abovedisplayskip 12\p@ \@plus3\p@ \@minus7\p@
   \abovedisplayshortskip \z@ \@plus3\p@
   \belowdisplayshortskip 6.5\p@ \@plus3.5\p@ \@minus3\p@
   \def\@listi{\leftmargin\leftmargini
               \topsep 9\p@ \@plus3\p@ \@minus5\p@
               \parsep 4.5\p@ \@plus2\p@ \@minus\p@
               \itemsep \parsep}%
   \belowdisplayskip \abovedisplayskip
}
\makeatother

% Increases space between footnote mark and text.
% See https://tex.stackexchange.com/a/54700.
\makeatletter
\long\def\@makefntext#1{%
  \parindent 1em\noindent \hb@xt@ 1.8em{\hss \@makefnmark}\hskip2mm\relax#1}%
\makeatother


%
% Title page
%

% Provides \includepdf command.
\usepackage{pdfpages}


%
% Table of contents, list of figures, list of tables
%

\usepackage{tocloft}

% Renders the ToC/LoF/LoT without title and on the same page.
% See https://tex.stackexchange.com/q/51479 and https://tex.stackexchange.com/q/173177.
\makeatletter
\newcommand{\scholar@tableofcontents}{\@starttoc{toc}}
\newcommand{\scholar@listoffigures}{\@starttoc{lof}}
\newcommand{\scholar@listoftables}{\@starttoc{lot}}
\makeatother

% Sets maximum depth of the ToC/LoF/LoT.
\setcounter{tocdepth}{3}  % include sections, subsections, and subsubsections in the ToC (ГОСТ 7.32-2017 5.4.1)
\setcounter{lofdepth}{1}  % include only figures (without subfigures) in the LoF
\setcounter{lotdepth}{1}  % include only tables (without subtables) in the LoT

% Removes the default vertical spacing between ToC/LoF/LoT entries.
\setlength{\cftbeforesecskip}{0pt}
\setlength{\cftbeforesubsecskip}{0pt}
\setlength{\cftbeforesubsubsecskip}{0pt}
\setlength{\cftbeforefigskip}{0pt}
\setlength{\cftbeforetabskip}{0pt}

% Removes the default indentation of ToC/LoF/LoT entries.
\setlength{\cftsecindent}{0pt}
\setlength{\cftsubsecindent}{0pt}
\setlength{\cftsubsubsecindent}{0pt}
\setlength{\cftfigindent}{0pt}
\setlength{\cfttabindent}{0pt}

% Sets the spacing between the ToC/LoF/LoT entry and the page number.
\setlength{\cftsecnumwidth}{0pt}
\renewcommand{\cftsecaftersnumb}{\hspace*{1.5em}}
\setlength{\cftsubsecnumwidth}{0pt}
\renewcommand{\cftsubsecaftersnumb}{\hspace*{2.5em}}
\setlength{\cftsubsubsecnumwidth}{0pt}
\renewcommand{\cftsubsubsecaftersnumb}{\hspace*{3.5em}}

% Adds a dot after the section/subsection/subsubsection number in the ToC.
\renewcommand{\cftsecaftersnum}{.}
\renewcommand{\cftsubsecaftersnum}{.}
\renewcommand{\cftsubsubsecaftersnum}{.}

% Enable the dot ledgers of ToC/LoF/LoT entries (ГОСТ 7.32-2017 5.4.1).
% Some had dot ledgers disabled by having "\cftnodots" instead of "\cftdotsep".
\renewcommand{\cftsecdotsep}{\cftdotsep}
\renewcommand{\cftsubsecdotsep}{\cftdotsep}
\renewcommand{\cftsubsubsecdotsep}{\cftdotsep}
\renewcommand{\cftfigdotsep}{\cftdotsep}
\renewcommand{\cfttabdotsep}{\cftdotsep}

% Styles titles as regular text (some titles were bold).
\renewcommand{\cftsecfont}{\normalfont}
\renewcommand{\cftsubsecfont}{\normalfont}
\renewcommand{\cftsubsubsecfont}{\normalfont}
\renewcommand{\cftfigfont}{\normalfont}
\renewcommand{\cfttabfont}{\normalfont}

% Styles dot ledgers as regular text (some dot leaders were bold).
\renewcommand{\cftsecleader}{\cftdotfill{\cftsecdotsep}}
\renewcommand{\cftsubsecleader}{\cftdotfill{\cftsubsecdotsep}}
\renewcommand{\cftsubsubsecleader}{\cftdotfill{\cftsubsubsecdotsep}}
\renewcommand{\cftfigleader}{\cftdotfill{\cftfigdotsep}}
\renewcommand{\cfttableader}{\cftdotfill{\cfttabdotsep}}

% Styles page numbers as regular text (some page numbers were bold).
\renewcommand{\cftsecpagefont}{\normalfont}
\renewcommand{\cftsubsecpagefont}{\normalfont}
\renewcommand{\cftsubsubsecpagefont}{\normalfont}
\renewcommand{\cftfigpagefont}{\normalfont}
\renewcommand{\cfttabpagefont}{\normalfont}


%
% Citation
%

% Creates a custom BibLaTeX entry type "scholar" that has only one
% field "text" that is used to store the rendered bibliography entry.
%
% The BibLaTeX entry types are defined in .dbx files. These instructions
% will create such file on the fly.
%
% Example:
%
%     @scholar{knuth84,
%       text = {Knuth, D. E. (1984). {\em The {\TeX}book}. Addison-Wesley.}
%     }
%
% See https://tex.stackexchange.com/a/175896.
\begin{filecontents}[overwrite]{scholar.dbx}
\DeclareDatamodelEntrytypes{scholar}
\DeclareDatamodelFields[type=field,datatype=literal]{text}
\DeclareDatamodelEntryfields[scholar]{text}
\end{filecontents}

\usepackage[
  backend=biber,      % explicitly use Biber as the backend
  datamodel=scholar,  % use the custom data model defined in "scholar.dbx"
  style=numeric,      % use numbers to cite the bibliography entries (GOST)
  autocite=inline,    % make "\autocite" behave like "\parencite"
  labelnumber=true,   % enable the special field "labelnumber"
  sorting=none,       % sort bibliography entries by the order they were cited (GOST)
  hyperref=true       % transform citations into clickable hyperlinks (requires the "hyperref" package)
]{biblatex}

% Formats text field as passthrough.
\DeclareFieldFormat[scholar]{text}{#1}

% Formats "scholar" entry types.
% Note that this bibliography driver doesn't respect BibLaTeX's before/after hooks because they add unwanted punctuation.
\DeclareBibliographyDriver{scholar}{\printfield{text}}

% Formats the bibliography entries using enumitem's list as a base.
% Needs package enumitem.
% Note that this bibenvironment doesn't respect BibLaTeX's field formatters because currently there was no way found to make them work with enumitem's list.
\defbibenvironment{scholar}
{\itemize[labelindent=0pt,leftmargin=*,listparindent=0pt,label={\protect\thefield{labelnumber}.},widest={0.}]}
{\enditemize}
{\item}

% Adds the .bib file with the bibliography entries.
\addbibresource{$scholar.constants.biblatex_bibresource$}


\begin{document}

$if(scholar.constants.includepdf_title_page)$
\includepdf[pages=-]{$scholar.constants.includepdf_title_page$}
$endif$

$body$

\end{document}
