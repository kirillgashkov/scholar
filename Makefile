format:
	autoflake -i -r --remove-all-unused-imports .
	isort --atomic .
	black .
	mypy .
.PHONY: fmt

check:
	autoflake --check -i -r --remove-all-unused-imports .
	isort --check --atomic .
	black --check .
	mypy .
.PHONY: lint

.PHONY: pip-compile
pip-compile:
	pip-compile --quiet --allow-unsafe --strip-extras --generate-hashes --output-file requirements.txt pyproject.toml
	pip-compile --quiet --allow-unsafe --strip-extras --generate-hashes --constraint requirements.txt --extra dev --output-file requirements-dev.txt pyproject.toml

.PHONY: pip-compile-upgrade
pip-compile-upgrade:
	pip-compile --upgrade --quiet --allow-unsafe --strip-extras --generate-hashes --output-file requirements.txt pyproject.toml
	pip-compile --upgrade --quiet --allow-unsafe --strip-extras --generate-hashes --constraint requirements.txt --extra dev --output-file requirements-dev.txt pyproject.toml

.PHONY: pip-sync
pip-sync:
	pip-sync requirements-dev.txt
